package com.brandonsblurays.movies.rest.handler;

import com.brandonsblurays.entity.movies.extended.MovieExtendedDetails;
import com.brandonsblurays.movies.rest.error.ServiceError;
import com.brandonsblurays.movies.rest.mapper.ExtendedMovieMapper;
import com.brandonsblurays.movies.rest.repository.MovieExtendedDetailsRepository;
import com.brandonsblurays.movies.rest.repository.MovieRepository;
import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieResponse;
import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieSearchResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ExtendedMovieHandler {

    @Value("${movieapikey}")
    private String movieApiKey;

    @Value("${movieapiurl}")
    private String movieApiUrl;

    private final String movieApiUrlBase = "/en/API";

    private RestTemplate restTemplate;
    private ExtendedMovieMapper extendedMovieMapper;
    private ObjectMapper objectMapper;

    public ExtendedMovieHandler(RestTemplate restTemplate, ExtendedMovieMapper extendedMovieMapper) {
        this.restTemplate = restTemplate;
        this.extendedMovieMapper = extendedMovieMapper;
        this.objectMapper = new ObjectMapper();
   }


   @Cacheable("extendedMoviesSearch")
   public List<ExtendedMovieSearchResponse> searchForMovies(String search) throws ServiceError {
        JsonNode rootNode = getResponseFromApi(movieApiUrl+movieApiUrlBase+"/Search/"+movieApiKey+"/"+search);

       if(rootNode.get("results") == null || rootNode.get("results").isEmpty())
           throw new ServiceError(400, "400", "No results found for the search");

        return extendedMovieMapper.mapSearchResponseFromJsonNode(rootNode);
   }

   @Cacheable("singleExtendedMovie")
   public ExtendedMovieResponse getSingleMovie(String movieId) throws ServiceError {
       JsonNode rootNode = getResponseFromApi(movieApiUrl+movieApiUrlBase+"/Title/"+movieApiKey+"/"+movieId);
       return extendedMovieMapper.mapExtendedMovieResponseFromJsonNode(rootNode);
   }

   public String reduceImageSize(String imageUrl) {
        //attempt to reduce the image size, if fails to return the original image size
        ResponseEntity<Object> response = restTemplate.getForEntity(movieApiUrl + "/API/ResizeImage?apikey=" + movieApiKey + "&size=288x433&url=" + imageUrl, Object.class);
        if (response.getStatusCodeValue() == 301 || response.getStatusCodeValue() == 302)
            return response.getHeaders().getLocation().toString();
        return imageUrl;
   }

    private JsonNode getResponseFromApi(String url) throws ServiceError {
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        if(response.getStatusCodeValue() != 200 || response.getBody() == null) {
            throw new ServiceError(500,"500", "Unexpected movie api error");
        }

        try {
            return objectMapper.readTree(response.getBody());
        } catch (JsonProcessingException e) {
            throw new ServiceError(500,"500", "Unexpected movie api error");
        }
    }

}
