package com.brandonsblurays.movies.rest.handler;

import com.brandonsblurays.entity.movies.Movie;
import com.brandonsblurays.entity.movies.extended.MovieExtendedDetails;
import com.brandonsblurays.movies.rest.repository.MovieExtendedDetailsRepository;
import com.brandonsblurays.movies.rest.repository.MovieRepository;
import com.brandonsblurays.movies.rest.error.ServiceError;
import com.brandonsblurays.movies.rest.mapper.MovieMapper;
import com.brandonsblurays.movies.rest.resource.resource.MovieResource;
import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieResponse;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
public class MovieHandler {

    private MovieRepository movieRepository;
    private MovieExtendedDetailsRepository movieExtendedDetailsRepository;
    private MovieMapper movieMapper;

    public MovieHandler(MovieRepository movieRepository,MovieExtendedDetailsRepository movieExtendedDetailsRepository, MovieMapper movieMapper) {
        this.movieRepository = movieRepository;
        this.movieMapper = movieMapper;
        this.movieExtendedDetailsRepository = movieExtendedDetailsRepository;
    }

    public List<MovieResource> getMovies() {
        List<Movie> movies = movieRepository.findAll();
        return movieMapper.mapToResources(movies);
    }

    public MovieResource getMovie(String movieId) {
        Movie movie = movieRepository.findById(movieId).get();
        MovieExtendedDetails extendedDetails = movieExtendedDetailsRepository.findById(movie.getExtendedDetailsId()).get();

        if(movie == null)
            return null;
        if(extendedDetails != null)
            return movieMapper.mapToResource(movie, extendedDetails);
        else
            return movieMapper.mapToResource(movie);
    }

    public String createMovie(MovieResource movieResource, ExtendedMovieResponse extendedMovieResponse) throws ServiceError {
        if(movieResource.getTitle() == null) {
            throw new ServiceError(400,"400","Must include title");
        }

        if(movieResource.getImageUrl() == null || movieResource.getImageUrl().isEmpty())
            movieResource.setImageUrl(extendedMovieResponse.getImage());

        Movie movie = movieMapper.mapToEntity(movieResource);
        movie.setUploadDate(new Date());
        movie.setMovieId(UUID.randomUUID().toString());

        long sortRanking = System.currentTimeMillis()/1000;

        movie.setSortRanking((int)sortRanking); //set the sort ranking to a high value so that way it shows up last
        movie.setMovieTitle(extendedMovieResponse.getTitle()); //overwrite title
        movie.setMovieImage(extendedMovieResponse.getImage()); //overwrite image with properly sized image

        MovieExtendedDetails extendedDetails = movieMapper.mapToEntity(extendedMovieResponse);

        movieExtendedDetailsRepository.save(extendedDetails);
        movieRepository.save(movie);
        return movie.getMovieId();
    }

    public void deleteMovie(String movieId) {
        //only delete the base movie, not the extended details
        movieRepository.deleteById(movieId);
    }

    public void updateMovie(String movieId, MovieResource resource) throws ServiceError{
        Movie movie = movieRepository.findById(movieId).get();
        if(movie == null) {
            throw new ServiceError(404, "404", "Movie id not found");
        }

        //can only update rating/toggles for type
        boolean anyUpdates = false;

        if(resource.getRating() != null) {
            movie.setMovieRating(resource.getRating());
            anyUpdates = true;
        }

        if(resource.isBluRay() != movie.isBluRay()) {
            movie.setBluRay(resource.isBluRay());
            anyUpdates = true;
        }

        if(resource.isDvd() != movie.isDvd()) {
            movie.setDvd(resource.isDvd());
            anyUpdates = true;
        }

        if(resource.isUltraHD() != movie.isUltraHD()) {
            movie.setUltraHD(resource.isUltraHD());
            anyUpdates = true;
        }

        if(anyUpdates)
            movieRepository.save(movie);
    }

}
