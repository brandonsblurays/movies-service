package com.brandonsblurays.movies.rest.repository;

import com.brandonsblurays.entity.movies.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovieRepository extends MongoRepository<Movie, String> {
}
