package com.brandonsblurays.movies.rest.repository;

import com.brandonsblurays.entity.movies.extended.MovieExtendedDetails;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovieExtendedDetailsRepository extends MongoRepository<MovieExtendedDetails, String> {
}
