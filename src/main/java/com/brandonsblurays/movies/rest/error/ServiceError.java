package com.brandonsblurays.movies.rest.error;

import lombok.Data;

@Data
public class ServiceError extends Exception {
    private int httpValue;
    private String errorCode;
    private String errorDetails;
    public ServiceError(int httpValue, String errorCode, String errorDetails) {
        this.httpValue = httpValue;
        this.errorCode = errorCode;
        this.errorDetails = errorDetails;
    }
}
