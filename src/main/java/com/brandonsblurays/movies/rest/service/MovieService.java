package com.brandonsblurays.movies.rest.service;

import com.brandonsblurays.movies.rest.handler.ExtendedMovieHandler;
import com.brandonsblurays.movies.rest.handler.MovieHandler;
import com.brandonsblurays.movies.rest.error.ServiceError;
import com.brandonsblurays.movies.rest.resource.resource.MovieResource;
import com.brandonsblurays.movies.rest.resource.response.BasicResponse;
import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {

    private MovieHandler movieHandler;
    private ExtendedMovieHandler extendedMovieHandler;

    public MovieService(MovieHandler movieHandler, ExtendedMovieHandler extendedMovieHandler) {
        this.movieHandler = movieHandler;
        this.extendedMovieHandler = extendedMovieHandler;
    }

    public List<MovieResource> getMovies() {
        return movieHandler.getMovies();
    }

    public MovieResource getMovie(String movieId) throws ServiceError {
        MovieResource resource = movieHandler.getMovie(movieId);
        if(resource == null) {
            throw new ServiceError(404,"404", "Not found");
        }
        return resource;
    }

    public BasicResponse createMovie(MovieResource movieResource) throws ServiceError {
        if(movieResource.getExternalId() == null)
            throw new ServiceError(400, "400", "Need an external ID");

        ExtendedMovieResponse extendedMovieResponse = extendedMovieHandler.getSingleMovie(movieResource.getExternalId());

        //reduce image size of extended movie details
        extendedMovieResponse.setImage(extendedMovieHandler.reduceImageSize(extendedMovieResponse.getImage()));



        String movieId = movieHandler.createMovie(movieResource, extendedMovieResponse);
        return new BasicResponse(movieId);
    }

    public void deleteMovie(String movieId) {
        movieHandler.deleteMovie(movieId);
    }

    public void updateMovie(String movieId, MovieResource movieResource) throws ServiceError {
        movieHandler.updateMovie(movieId, movieResource);
    }

}
