package com.brandonsblurays.movies.rest.service;

import com.brandonsblurays.movies.rest.error.ServiceError;
import com.brandonsblurays.movies.rest.handler.ExtendedMovieHandler;
import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieResponse;
import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieSearchResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExtendedMovieService {
    private ExtendedMovieHandler extendedMovieHandler;

    public ExtendedMovieService(ExtendedMovieHandler extendedMovieHandler) {
        this.extendedMovieHandler = extendedMovieHandler;
    }

    public List<ExtendedMovieSearchResponse> searchForMovies(String search) throws ServiceError {
        return extendedMovieHandler.searchForMovies(search);
    }

    public ExtendedMovieResponse getSingleMovie(String movieId) throws ServiceError {
        return extendedMovieHandler.getSingleMovie(movieId);
    }
}
