package com.brandonsblurays.movies.rest.controller;

import com.brandonsblurays.movies.rest.error.ServiceError;
import com.brandonsblurays.movies.rest.service.ExtendedMovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/extended/movies")
public class ExtendedMovieController extends BaseController {

    private ExtendedMovieService extendedMovieService;

    public ExtendedMovieController(HttpServletRequest request, ExtendedMovieService extendedMovieService) {
        super(request);
        this.extendedMovieService = extendedMovieService;
    }

    @GetMapping
    public ResponseEntity searchForMovies(@RequestParam String search) {
        try {
            return ResponseEntity.ok(extendedMovieService.searchForMovies(search));
        } catch(ServiceError e) {
            return serviceError(e);
        }
    }

    @GetMapping("/movie/{movieId}")
    public ResponseEntity getSingleMovieDetails(@PathVariable String movieId) {
        try {
            return ResponseEntity.ok(extendedMovieService.getSingleMovie(movieId));
        } catch(ServiceError e) {
            return serviceError(e);
        }
    }
}
