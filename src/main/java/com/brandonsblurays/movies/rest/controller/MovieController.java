package com.brandonsblurays.movies.rest.controller;

import com.brandonsblurays.movies.rest.error.ServiceError;
import com.brandonsblurays.movies.rest.resource.resource.MovieResource;
import com.brandonsblurays.movies.rest.service.MovieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/movies")
public class MovieController extends BaseController {

    private MovieService movieService;

    public MovieController(HttpServletRequest httpServletRequest, MovieService movieService) {
        super(httpServletRequest);
        this.movieService = movieService;
    }


    @GetMapping
    public ResponseEntity getMovies() {
        return ResponseEntity.ok(movieService.getMovies());
    }

    @GetMapping("/movie/{movieId}")
    public ResponseEntity getMovie(@PathVariable String movieId) {
        try {
            return ResponseEntity.ok(movieService.getMovie(movieId));
        } catch(ServiceError e) {
            return serviceError(e);
        }
    }

    @PostMapping
    public ResponseEntity createMovie(@RequestBody MovieResource movieResource) {
        if(!isAuthed())
            return serviceError(new ServiceError(401, "401", "Need to be logged in to create movies"));

        try {
            return ResponseEntity.ok(movieService.createMovie(movieResource));
        } catch(ServiceError e) {
            return serviceError(e);
        }
    }

    @PatchMapping("/movie/{movieId}")
    public ResponseEntity updateMovie(@PathVariable String movieId, @RequestBody MovieResource movieResource) {
        if(!isAuthed())
            return serviceError(new ServiceError(401, "401", "Need to be logged in to update movies"));
        try {
            movieService.updateMovie(movieId, movieResource);
            return ResponseEntity.ok(null);
        } catch(ServiceError e) {
            return serviceError(e);
        }
    }

    @DeleteMapping("/movie/{movieId}")
    public ResponseEntity deleteMovie(@PathVariable String movieId) {
        if(!isAuthed())
            return serviceError(new ServiceError(401, "401", "Need to be logged in to delete movies"));
        movieService.deleteMovie(movieId);
        return ResponseEntity.ok(null);
    }
}
