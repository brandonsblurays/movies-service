package com.brandonsblurays.movies.rest.controller;

import com.brandonsblurays.movies.rest.resource.response.BasicResponse;
import com.brandonsblurays.util.TokenUtil;
import com.brandonsblurays.movies.rest.error.ServiceError;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseController {
    private HttpServletRequest request;
    public BaseController(HttpServletRequest request) {
        this.request = request;
    }

    public ResponseEntity serviceError(ServiceError serviceError) {
        return ResponseEntity.status(serviceError.getHttpValue()).body(new BasicResponse(serviceError.getErrorDetails()));
    }

    public boolean isAuthed() {
        String headerValue = TokenUtil.extractTokenFromRequest(request);
        System.out.println(headerValue);
        System.out.println(TokenUtil.isTokenValid(headerValue));


        if(headerValue == null || !TokenUtil.isTokenValid(headerValue))
            return false;
        return true;
    }
}
