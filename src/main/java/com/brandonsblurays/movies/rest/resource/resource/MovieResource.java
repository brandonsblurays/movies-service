package com.brandonsblurays.movies.rest.resource.resource;

import lombok.Data;

@Data
public class MovieResource {
    private String id;
    private String externalId;
    private String title;
    private String rating;
    private String imageUrl;

    private boolean ultraHD;
    private boolean bluRay;
    private boolean dvd;

    private String uploadDate;

    private MovieExtendedDetailsResource extendedDetails;
}
