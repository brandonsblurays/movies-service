package com.brandonsblurays.movies.rest.resource.response;

import lombok.Data;

@Data
public class ExtendedMovieSearchResponse {
    private String id;
    private String image;
    private String title;
}
