package com.brandonsblurays.movies.rest.resource.response;

import lombok.Data;

import java.util.List;

@Data
public class ExtendedMovieResponse {
    private String id;
    private String title;
    private String image;
    private String runtimeStr;
    private String releaseDate;
    private List<String> directorList;
    private List<String> starsList;
    private List<String> writersList;
    private String imdbRating;
    private String metacriticRating;
    private String revenue;
}
