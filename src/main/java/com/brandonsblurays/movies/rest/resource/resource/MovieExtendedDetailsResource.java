package com.brandonsblurays.movies.rest.resource.resource;

import lombok.Data;

import java.util.List;

@Data
public class MovieExtendedDetailsResource {

    private String releaseDate;
    private List<String> stars;
    private List<String> writers;
    private List<String> directors;
    private float revenue;
    private String imDbRating;
    private String metacriticRating;
    private String runtimeString;

}
