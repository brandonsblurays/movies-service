package com.brandonsblurays.movies.rest.resource.response;

import lombok.Data;

@Data
public class BasicResponse {
    private String response;
    public BasicResponse(String response) {
        this.response = response;
    }
}
