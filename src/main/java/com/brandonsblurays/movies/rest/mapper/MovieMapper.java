package com.brandonsblurays.movies.rest.mapper;

import com.brandonsblurays.entity.movies.Movie;
import com.brandonsblurays.entity.movies.extended.MovieExtendedDetails;
import com.brandonsblurays.movies.rest.resource.resource.MovieExtendedDetailsResource;
import com.brandonsblurays.movies.rest.resource.resource.MovieResource;
import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieResponse;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class MovieMapper {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public List<MovieResource> mapToResources(List<Movie> movies) {
        List<MovieResource> resources = new ArrayList<>();
        for(Movie movie : movies)
            resources.add(mapToResource(movie));
        return resources;
    }

    public MovieResource mapToResource(Movie movie) {
        MovieResource resource = new MovieResource();
        resource.setId(movie.getMovieId());
        resource.setTitle(movie.getMovieTitle());
        resource.setRating(movie.getMovieRating());
        resource.setImageUrl(movie.getMovieImage());

        resource.setUltraHD(movie.isUltraHD());
        resource.setBluRay(movie.isBluRay());
        resource.setDvd(movie.isDvd());

        resource.setUploadDate(dateFormat.format(movie.getUploadDate()));

        return resource;
    }

    public MovieResource mapToResource(Movie movie, MovieExtendedDetails extendedDetails) {
        MovieResource movieResource = mapToResource(movie);
        movieResource.setExtendedDetails(mapToResource(extendedDetails));
        return movieResource;
    }

    private MovieExtendedDetailsResource mapToResource(MovieExtendedDetails extendedDetails) {
        MovieExtendedDetailsResource detailsResource = new MovieExtendedDetailsResource();
        detailsResource.setReleaseDate(extendedDetails.getReleaseDate());
        detailsResource.setStars(extendedDetails.getStars());
        detailsResource.setWriters(extendedDetails.getWriters());
        detailsResource.setDirectors(extendedDetails.getDirectors());
        detailsResource.setImDbRating(extendedDetails.getImDbRating());
        detailsResource.setMetacriticRating(extendedDetails.getMetacriticRating());
        detailsResource.setRuntimeString(extendedDetails.getRuntimeString());
        return detailsResource;
    }

    public Movie mapToEntity(MovieResource resource) {
        Movie movie = new Movie();
        movie.setMovieTitle(resource.getTitle());
        movie.setMovieRating(resource.getRating());
        movie.setMovieImage(resource.getImageUrl());
        movie.setExtendedDetailsId(resource.getExternalId());

        movie.setUltraHD(resource.isUltraHD());
        movie.setBluRay(resource.isBluRay());
        movie.setDvd(resource.isDvd());
        return movie;
    }

    public MovieExtendedDetails mapToEntity(ExtendedMovieResponse resource) {
        MovieExtendedDetails details = new MovieExtendedDetails();
        details.setExtendedDetailsId(resource.getId());
        details.setMovieImage(resource.getImage());
        details.setReleaseDate(resource.getReleaseDate());
        details.setStars(resource.getStarsList());
        details.setWriters(resource.getWritersList());
        details.setDirectors(resource.getDirectorList());
        details.setImDbRating(resource.getImdbRating());
        details.setMetacriticRating(resource.getMetacriticRating());
        details.setRuntimeString(resource.getRuntimeStr());
        return details;
    }


}
