package com.brandonsblurays.movies.rest.mapper;

import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieResponse;
import com.brandonsblurays.movies.rest.resource.response.ExtendedMovieSearchResponse;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExtendedMovieMapper {

    public List<ExtendedMovieSearchResponse> mapSearchResponseFromJsonNode(JsonNode rootNode) {
        List<ExtendedMovieSearchResponse> responses = new ArrayList<>();

        for(JsonNode node : rootNode.get("results")) {
            ExtendedMovieSearchResponse response = new ExtendedMovieSearchResponse();
            response.setId(node.get("id").asText());
            response.setImage(node.get("image").asText());
            response.setTitle(node.get("title").asText());
            responses.add(response);
        }
        return responses;
    }

    public ExtendedMovieResponse mapExtendedMovieResponseFromJsonNode(JsonNode rootNode) {
        ExtendedMovieResponse response = new ExtendedMovieResponse();
        response.setId(rootNode.get("id").asText());
        response.setTitle(rootNode.get("title").asText());
        response.setImage(rootNode.get("image").asText());
        response.setReleaseDate(rootNode.get("releaseDate").asText());
        response.setRuntimeStr(rootNode.get("runtimeStr").asText());
        response.setRevenue(rootNode.get("boxOffice").get("cumulativeWorldwideGross").asText());

        List<String> directorList = new ArrayList<>();
        for(JsonNode node : rootNode.get("directorList")) {
            directorList.add(node.get("name").asText());
        }

        List<String> writerList = new ArrayList<>();
        for(JsonNode node : rootNode.get("writerList")) {
            writerList.add(node.get("name").asText());
        }

        List<String> starList = new ArrayList<>();
        for(JsonNode node : rootNode.get("starList")) {
            starList.add(node.get("name").asText());
        }

        response.setDirectorList(directorList);
        response.setStarsList(starList);
        response.setWritersList(writerList);

        response.setImdbRating(rootNode.get("imDbRating").asText());
        response.setMetacriticRating(rootNode.get("metacriticRating").asText());

        return response;
    }


}
